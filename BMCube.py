import Tkinter
import tkFont

import pandas as pd
import numpy as np
from collections import defaultdict

window=Tkinter.Tk()
window.geometry("500x500")
window.title("DELL")
helv36 = tkFont.Font(family='Helvetica', size=16, weight=tkFont.BOLD)
helv12 = tkFont.Font(family='Times', size=12, weight=tkFont.BOLD)
time12 = tkFont.Font(family='Times', size=14, weight=tkFont.BOLD)
label=Tkinter.Label(window,text="WELCOME TO DELL-LAPTOPS",font=helv36).pack()
#label2=Tkinter.Label(window,text="expected cost",font=time12).place(x=45,y=30)
label1=Tkinter.Label(window,text="SPECIFICATIONS\nplatform : Windows 10\nprocessor name : Intel core I5\nprocessor speed : 2.6 GHZ\nRAM : 8GB\nstorage : 1TB\nscreen size : 17 inches\nresolution : 3480 x 2160 \nweight : 2.7 lb\nBattery : 11.10 \nkeyboard-1",font=("arial",12,"bold")).place(x=120,y=260)
label2=Tkinter.Label(window,text="SPECIFICATIONS\nplatform : Windows 10\nprocessor name : Intel core I9\nprocessor speed : 3.6 GHZ\nRAM : 16GB\nstorage : 2TB\nscreen size : 17 inches\nresolution : 1920 X 1080 \nweight : 9 lb\nBattery : 02:06 \nkeyboard-1",font=("arial",12,"bold")).place(x=610,y=260)
label3=Tkinter.Label(window,text="SPECIFICATIONS\nplatform : Windows 10\nprocessor name : Intel core I7\nprocessor speed : 3.1 GHZ\nRAM : 16GB\nstorage : 512 GB\nscreen size : 14 inches\nresolution : 1920 X 1080 \nweight : 4.98 lb\nBattery: 05:22 \nkeyboard-2",font=("arial",12,"bold")).place(x=1100,y=260)

 
lb1=Tkinter.Label(window,text="choose a laptop",font=helv36).place(x=50,y=70)
def choselaptop1():
    laptopval = 0
    fun(laptopval)
def choselaptop2():
    laptopval = 1
    fun(laptopval)
def choselaptop3():
    laptopval = 2
    fun(laptopval)

def fun(laptopval):
    laptopnames=['LAPTOP-1','LAPTOP-2','LAPTOP-3']
    df=pd.read_excel(r'C:\Users\HP\Desktop\BOM-pre-edit-1.xlsx')
    L1=pd.read_excel(r'C:\Users\HP\Desktop\L1.xlsx')
    k=np.asarray(L1[laptopnames[laptopval]])
    def mklist(n):
        for _ in range(n):
            yield []

    p1=[]
    p2=[]
    p3=[]
    p4=[]
    A=[]
    B=[]
    temp=[]

    def KMaxCombinations( A, B, p1, p2, K): 
        for i in range(0, len(A)): 
            for j in range(0, len(B)): 
                f4 = []             
                a = A[i] + B[j]
                b=p1[i]+p2[j]
                f4.append(a)
                f4.append(A[i])
                f4.append(B[j])
                f4.append(b)
                f4.append(p1[i])
                f4.append(p2[j])
                temp.append(f4)
             

    df1=[]
    for i in range(len(k)):
        df1.append(df[df['commodity'].str.match(k[i])])
    kk=pd.concat(df1[0:len(k)+1])
    z1=max(np.asarray(kk['Level']).astype(int))
    w= kk.loc[(kk['Level'] >= z1)] 
    k2=w['Level'].unique()
    f=((10-kk['s.p.r'])**2)+((10-kk['c.p.r'])**2)
    f1=((10-w['s.p.r'])**2)+((10-w['c.p.r'])**2)
    kk['sum']= np.array(f)
   
    pos1=kk['Level'].unique()
    yu=np.array(kk)


    for r in range (0,len(yu)):
        if (yu[r]==k2[0]).any():
            p1.append(yu[r][8])
            A.append(yu[r][5])
        if (yu[r]==k2[1]).any():
            p2.append(yu[r][8])
            B.append(yu[r][5])


    K = len(A)*len(B)
    KMaxCombinations( A, B, p1, p2, K)  
    df9 = pd.DataFrame(temp,columns=['sum_tot','sum_1','sum_2','b','dis1','dis2'])
       
    df9.sort_values("b",ascending = True,inplace=True,na_position='last')
    res2=[j for j in range (0,len(df9['b'])) if df9['b'][j]==min(df9['b'])]
#res2.append(3)
    minval = res2[0]
    for loopvar in res2:
        if (df9['sum_tot'][loopvar]<df9['sum_tot'][minval]):
            minval = loopvar
    mini=min(df9['b'])
    res=[h for h in range (len(df9)) if df9['b'][h]== mini]
    cost_of_mother_board=temp[res[0]][0]  
    cost_of_CPU=temp[res[0]][1]
    cost_of_RAM=temp[res[0]][2] 

#level-3
    z2=z1-1
    w3=kk.loc[( kk['Level'] >z2)].loc[( kk['Level'] <z1)]['Level'].unique()
    p5=[]
    p6=[]
    p7=[]
    C=[]
    D=[]
    E=[]
    temp1=[]
    for e in range (0,len(yu)):
        if (yu[e]==w3[0]).any():
            p5.append(yu[e][8])
            C.append(yu[e][5])
        if (yu[e]==w3[1]).any():
            p6.append(yu[e][8])
            D.append(yu[e][5])
        if (yu[e]==w3[2]).any():
            p7.append(yu[e][8])
            E.append(yu[e][5])
    def KMaxCombinations(C, D, E, p5, p6, p7, K1): 
        for m in range (0,len(C)):
            for n in range (0,len(D)):
                for x in range (0,len(E)):
                    f6=[]
                    c=C[m] + D[n] + E[x]  
                    d=p5[m]+p6[n]+p7[x]
                    f6.append(c)
                    f6.append(C[m])
                    f6.append(D[n])
                    f6.append(E[x])
                    f6.append(d)
                    f6.append(p5[m])
                    f6.append(p6[n])
                    f6.append(p7[x])
                    temp1.append(f6)
    K1=len(C)*len(D)*len(E)*len(p5)*len(p6)*len(p7)
    KMaxCombinations(C, D, E, p5, p6, p7, K1)
    df10 = pd.DataFrame(temp1,columns=['sum_tot','sum_1','sum_2','sum_3', 'c','dis1','dis2', 'dist3']) 
    df10.sort_values("c",ascending = True,inplace=True,na_position='last')
    res_1=[j for j in range (0,len(df10['c'])) if df10['c'][j]==min(df10['c'])]
#res2.append(3)
    minval = res_1[0]
    for loopvar in res_1:
        if (df10['sum_tot'][loopvar]<df10['sum_tot'][minval]):
            minval = loopvar

    mini1=min(df10['c'])
    res1=[h for h in range (len(df10)) if df10['c'][h]== mini1]
    cost_of_REM=temp1[res1[0]][0]  
    cost_of_GC=temp1[res1[0]][1]
    cost_of_HDD=temp1[res1[0]][2] 
    cost_of_OP=temp1[res1[0]][3]
    cost_of_basepanel=cost_of_REM+cost_of_mother_board

#level2
    z3=z2-1
    w4=kk.loc[( kk['Level'] >z3)].loc[( kk['Level'] <z2)]['Level'].unique()
    F=[]
    p8=[]
    p9=[]
    G=[]
    for r in range (0,len(yu)):
        if (yu[r]==w4[0]).any():
            p8.append(yu[r][8])
            F.append(yu[r][5])
        if (yu[r]==w4[1]).any():
            p9.append(yu[r][8])
            G.append(yu[r][5])
    temp3=[]
    def KMaxCombinations( F, G, p8, p9, K4): 
        for z in range(0, len(F)): 
             for v in range(0, len(G)): 
                 f11 = []             
                 r5 = F[z] + G[v]
                 r6=p8[z]+p9[v]
                 f11.append(r5)
                 f11.append(F[z])
                 f11.append(G[v])
                 f11.append(r6)
                 f11.append(p8[z])
                 f11.append(p9[v])
                 temp3.append(f11)
    K4=len(F)*len(G)*len(p8)*len(p9)
    KMaxCombinations(F, G, p8, p9, K4)
    df11 = pd.DataFrame(temp3,columns=['r5','sum_1','sum_2', 'r6','dis1','dis2']) 
    df11.sort_values("r6",ascending = True,inplace=True,na_position='last')
    res_3=[j for j in range (0,len(df11['r6'])) if df11['r6'][j]==min(df11['r6'])]
#res2.append(3)
    minval = res_3[0]
    for loopvar in res_3:
        if (df11['r5'][loopvar]<df11['r5'][minval]):
            minval = loopvar

    mini3=min(df11['r6'])
    res3=[h for h in range (len(df11)) if df11['r6'][h]== mini3]
    cost_of_REM2=temp3[res3[0]][0] 
    cost_of_screen=temp3[res3[0]][1]
    cost_of_keyboard=temp3[res3[0]][2]
    cost_of_laptop=cost_of_basepanel+cost_of_REM2
    print (cost_of_laptop)
    #return cost_of_laptop

####################MULTILEVELS###################
    a5=[]
    def y(arr):
        n = len(arr)
        indices = [0 for i in range(n)]
        while (1):
            a9=[]
            for i in range(n):            
                a9.append(arr[i][indices[i]])
            a5.append(a9)
   
            next = n - 1
            while (next >= 0 and 
                  (indices[next] + 1 >= len(arr[next]))):
                next-=1
            if (next < 0):
                return
            indices[next] += 1
            for i in range(next + 1, n):
                indices[i] = 0


    k11=kk.sort_values("Level",ascending=True)
    r13=k11['commodity'].unique()
    reso=[]
    for i in range(0,len(r13)):
        reso.append([m1 for m1 in range (0,len(df['commodity'])) if df['commodity'][m1]== r13[i]])
    reso1=[]
    reso2=[]
    reso3=[]
    df['add']=(10-df['s.p.r'])**2+(10-df['c.p.r'])**2
    for j in range(0,len(reso)):
            reso1.append(df['cost (INR)'][reso[j][0:len(reso[j])]])
            reso2.append(df['add'][reso[j][0:len(reso[j])]])

    arr=[]
    ant1=[]
    arr1=[]
    for j in range(0,len(reso1)):
        arr.append(reso1[j].tolist())
        arr1.append(reso2[j].tolist())
    #arr.append(ant1[j])
#arr=[A,B,C,D,E,F,F1]
#arr=arr9
    n=len(arr)
    y(arr)
    var=a5
    var1=pd.DataFrame(var,columns=['screen','keyboard','GC','HDD','OP','CPU','RAM'])
  
    po=[]
    for i in range(0,len(a5)):
        po.append(sum(a5[i]))


    a5=[]

#arr1=[h1,h2,h3,h4,h5,h6,h7]
    n=len(arr1)
    y(arr1)
    var2=a5
    var3=pd.DataFrame(var,columns=['h1','h2','h3','h5','h6','h7','h8'])
    po1=[]
    for j in range(0,len(a5)):
        po1.append(sum(a5[j]))


    sum_array=pd.DataFrame(po,columns=['sumA'])
    dist_array=pd.DataFrame(po1,columns=['distsumB'])
    dist_sorted=dist_array.sort_values("distsumB",ascending=True)
    dr=dist_sorted['distsumB'].unique()
    res4=[]
    for i in range(1,20):
        res4.append([m1 for m1 in range (0,len(dist_sorted['distsumB'])) if dist_sorted['distsumB'][m1]== dr[i]])
    flat_list = []
    for sublist in res4:
        for item in sublist:
            flat_list.append(item)
    rows=sum_array.loc[flat_list[0:len(flat_list)]]
    rows1=dist_sorted.loc[flat_list[0:len(flat_list)]]       
    div1=max(rows['sumA'])
    div2=max(rows1['distsumB'])
    rows['sum_div']=rows['sumA']/div1
    rows['sum_div1']=rows1['distsumB']/div2
    rows['comp']=(1-rows['sum_div'])**2+(1-rows['sum_div1'])**2
    final=max(rows['comp'])
    k7=rows.loc[rows['comp'].idxmax()]
    print k7

    k8=[m1 for m1 in range (0,len(sum_array['sumA'])) if sum_array['sumA'][m1]==k7[0]]
#k9=.loc[rows['comp']
    rows78 = var1.loc[k8[0]]
    y=var[k8[0]]

#for i in range(len(kk)):
#k12=[]
    y2=[]

    for i in range(0,len(df['cost (INR)'])):
        for j in range(len(y)):
            if (df['cost (INR)'][i]==y[j]):
                y2.append(i)
    BOMTWO = df.loc[y2]  
    print BOMTWO
    #return BOMTWO
    k29=[m1 for m1 in range (0,len(sum_array['sumA'])) if sum_array['sumA'][m1]==cost_of_laptop]
    rows79 = var1.loc[k29[0]]
    y=var[k29[0]]
    print "let's see next BOM"

    y22=[]

    for i in range(0,len(df['cost (INR)'])):
        for j in range(len(y)):
            if (df['cost (INR)'][i]==y[j]):
                y22.append(i)
    BOMONE= df.loc[y22]  
    print (BOMONE)
    #return BOMONE
    maximum_sum=max(po)
    print "The maximum cost is"
    print (maximum_sum)
    return maximum_sum ,BOMONE ,BOMTWO , cost_of_laptop,k7

#BOMTWO = fun(0)
def f_events():
    maximum_sum ,BOMONE ,BOMTWO , cost_of_laptop,k7 = fun(0) 
    
    window2=Tkinter.Tk()
    label_c = Tkinter.Label(window2, text= cost_of_laptop).pack()
    label_c= Tkinter.Label(window2, text= BOMONE).pack()
    label_c = Tkinter.Label(window2, text= k7).pack()
    label_c = Tkinter.Label(window2, text= BOMTWO).pack()# BOMONE
    label_c = Tkinter.Label(window2, text= maximum_sum).pack()
  
def f1_events():
    maximum_sum ,BOMONE ,BOMTWO , cost_of_laptop,k7 = fun(1) 
    window2=Tkinter.Tk()
    label_c = Tkinter.Label(window2, text= cost_of_laptop).pack()
    label_c= Tkinter.Label(window2, text= BOMONE).pack()
    label_c = Tkinter.Label(window2, text= k7).pack()
    label_c = Tkinter.Label(window2, text= BOMTWO).pack()# BOMONE
    label_c = Tkinter.Label(window2, text= maximum_sum).pack()

def f2_events():
    maximum_sum ,BOMONE ,BOMTWO , cost_of_laptop,k7 = fun(2) 
    window2=Tkinter.Tk()
    label_c = Tkinter.Label(window2, text= cost_of_laptop).pack()
    label_c= Tkinter.Label(window2, text= BOMONE).pack()
    label_c = Tkinter.Label(window2, text= k7).pack()
    label_c = Tkinter.Label(window2, text= BOMTWO).pack()# BOMONE
    label_c = Tkinter.Label(window2, text= maximum_sum).pack()
   
btn1=Tkinter.Button(text='LAPTOP-1', font=helv12,command=choselaptop1).place(x=180,y=200)
btn1=Tkinter.Button(text='LAPTOP-1', font=helv12,command=f_events).place(x=180,y=200)
btn2=Tkinter.Button(text='LAPTOP-2', font=helv12,command=choselaptop2).place(x=680,y=200)
btn2=Tkinter.Button(text='LAPTOP-2', font=helv12,command=f1_events).place(x=680,y=200)
btn3=Tkinter.Button(text='LAPTOP-3', font=helv12,command=choselaptop3).place(x=1180,y=200)
btn3=Tkinter.Button(text='LAPTOP-3', font=helv12,command=f2_events).place(x=1180,y=200)


window.mainloop()

 
